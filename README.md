# Project 4: Data Pipeline in Rust and AWS

Zach Xing

## Exploring Data Transformation with Rust & AWS

### Introduction

In this exploration, we delve into crafting a robust data processing pipeline using Rust and AWS Lambda functions, orchestrated seamlessly by AWS Step Functions. Our endeavor involves orchestrating a series of Lambda functions to undertake diverse data preprocessing tasks.

### Project Journey

#### Rust Lambda Utilities

We've harnessed the power of Rust to create Lambda functions tailored for our data journey.

- **Text Transformation**: This function humbly converts text to lowercase, ensuring uniformity and easing downstream processing.
  
- **Punctuation Purge**: Another Lambda gem, this function delicately removes unnecessary punctuation marks, refining our text for focused analysis.

To embark on this journey, we summon Rust's magic:

```bash
cargo lambda new text_to_lowercase
cargo lambda new remove_punctuation
```

With each function meticulously crafted, we validate their prowess:

```bash
cd text_to_lowercase
cargo lambda watch

cd remove_punctuation
cargo lambda watch
```

and then we test the functions:

```bash
cargo lambda invoke --data-file ./data_folder/data1.json
cargo lambda invoke --data-file ./data_folder/data2.json
```

![](./assets/1.png)

#### AWS Lambda Deployment

Taking flight into the AWS cloud, we deploy our Lambda functions:

```bash
cargo lambda build --release
cargo lambda deploy --region us-east-1 --iam-role (input your role here)
```

![](./assets/2.png)
![](./assets/3.png)

### Crafting the Symphony: Step Functions

Our journey's heart lies in the orchestration of Step Functions. Here's the unfolding tale:

1. **Step Function Choreography**: Crafting a dance of state machines choreographed with Lambda functions.

```json
{
  "Comment": "Text transformation pipeline: Lowercasing and punctuation removal.",
  "StartAt": "TextToLowercase",
  "States": {
    "TextToLowercase": {
      "Type": "Task",
      "Resource": "(your function arn here)",
      "Next": "RemovePunctuation"
    },
    "RemovePunctuation": {
      "Type": "Task",
      "Resource": "(your function arn here)",
      "End": true
    }
  }
}
```

![Harmony](./images/5.png)

2. **Commencing the Symphony**: Initiating the grand performance with a harmonious data input:

{
    "data": "creaTivity IS just cOnnecting thiNGs. whEn you ASK CREative people HOW they DID something, thEy FEel a little guilty BEcause THEY didn't really DO it, thEy JUST saw SOMETHING. It sEEmed obvious TO them AFTER a while."
}

![](./assets/4.png)


## Crafting the Pipeline
Witness the transformation within the TextToLowercase realm, followed by the purification in the RemovePunctuation sanctum. Behold, a majestic pipeline unfolds. Witness its essence captured below. The lowercase state is born, untainted by capitals yet bound by punctuation.

## Demo

<video width="320" height="240" controls>
  <source src="assets/Demo_IP4.mov" type="video/mp4">
Your browser does not support the video tag.
</video>


If the video does not play, you can view it at (assets/Demo_IP4.mov), or you can download the video [here](assets/Demo_IP4.mov).


